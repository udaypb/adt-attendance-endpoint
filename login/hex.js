var sha256 = require('sha256');
var auth = require('../auth/Credential');

var exports = module.exports = {};

exports.generateSID = function(emailAddress)
{
	var emailHex = sha256(emailAddress);
	var serverHex = sha256(auth.ServerID);
	//console.log('Email Hex : ' + emailHex);
	//var m1 = emailHex + ;
	//console.log('M1 : ' + m1);
	var sid = sha256(emailHex + serverHex);

	return sid;
}

exports.generateAccessToken = function(regno, sid)
{

	var regnoHex = sha256(regno);
	var timeNow = Date.now();
	//One Hour AccessToken Timeout
	var timeTill = timeNow + 3600000;
	// console.log(timeNow);
	// console.log(timeTill);

	var timeNowHex = sha256(timeNow.toString());
	//console.log('TimeNow Hex : '+ timeNowHex);
	var accessToken = sha256(regnoHex+sid+timeNowHex);
	var returnVar = {'accessToken': accessToken, 'validity':timeTill};
	return returnVar;
}