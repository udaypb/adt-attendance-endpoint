var express = require('express');
var app = express();
var login = require('./login/login');
var fs = require('fs');
var path = require('path');
var attendance=require('../attendance/attendance');

app.get('/credentialLogin',login.credentialLogin);
app.get('/googleLogin', login.googleLogin);
app.get('/attendance',attendance.check);
app.get('/', function(request, response, next)
{
	var filePath = "./public/login.html";
	var readStream = fs.createReadStream(filePath);
	readStream.pipe(response);
})
app.listen(3000);

app.get('/home', function(request, response, next)
{
	
});


//Middlewaress ___----____

app.use(express.static('./public'));
