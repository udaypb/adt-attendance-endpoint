var http = require('http');
var permissionUrl = {
  "permission": {
    "id": "some id",
    "url": "/Portal/api/sample.js"
  }
};

http.createServer(function (request, response) {

   // Send the HTTP header 
   // HTTP Status: 200 : OK
   // Content Type: text/plain
   response.writeHead(200, {'Content-Type': 'application/json'});
   
   // Send the response body as "Hello World"
   response.end(JSON.stringify(permissionUrl));
}).listen(8081);

// Console will print the message
console.log('Server running at http://127.0.0.1:8081/');